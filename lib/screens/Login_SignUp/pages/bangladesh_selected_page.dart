import 'dart:convert';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/otp_generate.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';

class BangladeshSelectedPage extends StatefulWidget {
  BangladeshSelectedPage({super.key, this.emailorpn});

  String? emailorpn;

  @override
  State<BangladeshSelectedPage> createState() => _BangladeshSelectedPageState();
}

class _BangladeshSelectedPageState extends State<BangladeshSelectedPage> {
  bool _obscureText1 = true;
  bool _obscureText2 = true;

  bool isRemembered = false;
  double textFormFieldHeight = 45.0;

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }
  final box=GetStorage();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  final _formkey = GlobalKey<FormState>();

  @override
  void initState() {
    phoneController.text = "${widget.emailorpn}";
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(20.0),
        color: Colors.white54,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Form(
            key: _formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Your Name *",
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
               const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 40,
                  child: TextFormField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      filled: true,
                      errorStyle:  TextStyle(fontSize: 0.01),
                      contentPadding: EdgeInsets.only(
                          left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(),
                      hintText: "name",
                      hintStyle: TextStyle(fontSize: 14),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'invalid';
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Your Phone *",
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    controller: phoneController,
                    keyboardType: TextInputType.phone,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "invalid";
                      }
                    },
                    decoration: InputDecoration(
                      filled: true,
                      errorStyle: const TextStyle(fontSize: 0.01),
                      contentPadding: EdgeInsets.only(
                          left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(),
                      hintText: "phone",
                      hintStyle: TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Password *',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _obscureText1,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        errorStyle: const TextStyle(fontSize: 0.01),
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText1
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle1();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "invalid";
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Confirm Password *',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    controller: confirmPasswordController,
                    obscureText: _obscureText2,
                    decoration: InputDecoration(
                        errorStyle: const TextStyle(fontSize: 0.01),
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText2
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle2();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "invalid";
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      activeColor: Colors.black,
                      value: isRemembered,
                      onChanged: (bool? value) {
                        setState(() {
                          isRemembered = value!;
                        });
                      },
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Row(
                      children: [
                        Text(
                          'I agree to the ',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Text(
                          'privacy policy',
                          style: GoogleFonts.poppins(
                            decoration: TextDecoration.underline,
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Text(
                          ' *',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formkey.currentState!.validate() &&
                        isRemembered == true) {
                      Fatch_registration();
                    }
                    else{
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content:Text("Agree Terms & Condition"),
                        ),
                      );
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'SIGN UP',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth / 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Icon(
                        Icons.forward,
                        color: Colors.white,
                      )
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Already have an account?'),
                    SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: (() {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignInPage(
                                  isRemembered: false,
                                  emphone: "01",
                                )),
                            (route) => false);
                      }),
                      child: Text(
                        'Sign In',
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: screenWidth / 24,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Fatch_registration() async {
    String link = "${Baseurl}add_web_customer";
    try {
      Response response = await Dio().post(link, data: {
        "customer": {
          "bound": "Bangladesh",
          "Customer_Name": "${nameController.text}",
          "Customer_Mobile": "${phoneController.text}",
          "Customer_Email": null,
          "country_id": "+880",
          "password": "${passwordController.text}",
          "confirm_password":"${confirmPasswordController.text}",
          "referral_code": null,
          "agreement": true,
        }
      });
      print(response.data);
      var item=jsonDecode(response.data);
      if(item["create"] ==true){
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PinputExample(
          number: phoneController.text,
        ),));
        box.write("customer_id", item["customer_id"]);
        box.write("customer_Mobile", phoneController.text);
        box.write("customer_name", nameController.text);
      }
      if(item["exists"] ==true){
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignInPage(
          emphone: phoneController.text,
          isRemembered: false,
        ),));
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Already you have an accoun"),
          ),
        );
      }
      print(item);
      print("exists ${item["exists"]}");
      print("create ${item["create"]}");
      print("customer_id ${item["customer_id"]}");
      print("customer_id ${box.read("customer_id")}");
      print("deactive ${item["deactive"]}");
    } catch (e) {
      print(e);
    }
  }

}
