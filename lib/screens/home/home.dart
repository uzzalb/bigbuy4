
import 'package:badges/badges.dart';
import 'package:bigbuy/Api_Integration/api_integration_all_product.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/CustomSectionPart/custom_section_part.dart';
import 'package:bigbuy/Custom/Custom_Appbar/custom_appbar.dart';
import 'package:bigbuy/Custom/Custom_Card/custom_card.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/Provider/SubCategory_Provider/all_sub_category_product.dart';
import 'package:bigbuy/screens/All_Category/SubCategoryProduct/sub_category_product.dart';
import 'package:bigbuy/screens/All_Category/all_category.dart';
import 'package:bigbuy/screens/One_Product_Details/one_product_details.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  final String? title;

  const HomeScreen({super.key, this.title});

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  int activePage = 1;
  List itemColor = [
    "CategoryIcon/SliderHome/bigbuyhome22.PNG",
    "CategoryIcon/SliderHome/bigbuyhome.PNG",
    "CategoryIcon/SliderHome/bigbuyhome22.PNG"
  ];



  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  void initState() {
    Api_All_Product_Integration.getAllCategoryNameList(context);
    Provider.of<All_Product_Provider>(context,listen: false).get_All_New_Arrivel_Product(context);
    Provider.of<All_Product_Provider>(context,listen: false).get_All_hot_Deal_Product_List(context);
    Provider.of<All_Product_Provider>(context,listen: false).getCategory_name_List(context);
    Provider.of<All_Product_Provider>(context,listen: false).getBrand(context);
    Provider.of<All_Product_Provider>(context,listen: false).getCategories(context);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    //
    // print("key ${GetStorage().read("key")}");
    // print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    // print("Customer_Name ${GetStorage().read("Customer_Name")}");
    // print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    // print("Customer_Address ${GetStorage().read("Customer_Address")}");
    // print("country_id ${GetStorage().read("country_id")}");
    // print("image_name ${GetStorage().read("image_name")}");
    // print("Customer_Email ${GetStorage().read("Customer_Email")}");
    // print("wallet_balance ${GetStorage().read("wallet_balance")}");
    // print("referral_id ${GetStorage().read("referral_id")}");


    final All_new_arrivel_product_list=Provider.of<All_Product_Provider>(context).all_new_arrivel_product_List;
  // final HotDelaProductList=Provider.of<All_Product_Provider>(context).all_hotdeal_product;
    final all_category_name_list=Provider.of<All_Product_Provider>(context).CategoryNameList;
 // final all_brand_list=Provider.of<All_Product_Provider>(context).brandList;
    final all_productlist=Provider.of<All_Product_Provider>(context).allproductlist;
    //Size size = MediaQuery.of(context).size;
    const padding = EdgeInsets.only(
      top: 5,
      left: 18,
      right: 18,
      bottom: 0,
    );
    print("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL ${box!.length}");
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            _key.currentState!.openDrawer();
          },
          icon: Icon(
            Icons.menu,
            size: 25,
            color: Colors.black87,
          ),
        ),
        title: Text(
          "BigBuy",
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontStyle: FontStyle.italic,
            letterSpacing: 1,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(
              Icons.search,
              size: 25,
              color: Colors.black87,
            ),
            onPressed: () {},
          ),
          Badge(
            position: BadgePosition.custom(
              top: 1,
              end: 5,
            ),
            badgeContent: Text("${box!.length}"),
            child: IconButton(
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
                icon: Icon(
                  Icons.shopping_cart,
                  size: 25,
                  color: Colors.black87,
                )),
          ),
        ],
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      key: _key,
      drawer: Custom_Drawer_Page(),
      endDrawer: End_Add_to_cart_Drawer(),

      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers:[
          ////////////////////////Home App Bar ////////////////
          // Custom_Appbar(
          //   onPressed: () {
          //     _key.currentState!.openDrawer();
          //   },
          //   End_Add_To_Cart_Drawer: () {
          //     _key.currentState!.openEndDrawer();
          //   },
          // ),
          ////////////////////////Logo sett ///////////////////
          SliverToBoxAdapter(
            child: Container(
              height: 50,
              width: double.infinity,
              alignment: Alignment.center,
              child: Image.asset(
                'CategoryIcon/bigbuy_logo.png',
                height: 40,
                width: 120,
              ),
            ),
          ),
          ////////////////////////Img Slide///////////////////
          SliverToBoxAdapter(
            child: CarouselSlider.builder(
            itemCount: itemColor.length,
              itemBuilder: (context, index, realIndex) {
                return Container(
                  height: 100,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage(itemColor[index]),fit: BoxFit.fill),
                  ),
                );
              },
              options: CarouselOptions(
                  enlargeCenterPage: false,
                  viewportFraction: 1,
                  autoPlay: true, autoPlayInterval: Duration(seconds: 3)),
            ),
          ),
          /////////////////////////////Category Slide /////////////////
          SliverPadding(
            padding: padding,
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Column(
                  children: [
                    Column(

                      children: [
                        SizedBox(height: 10),
                        /////////////////////////////Home Categoris //////////////////////
                        InkWell(
                          onTap:(){
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => All_CateGory_Page(),));
                          },
                          child: CustomSectionPart(
                            FristPart: "Explore Categories",
                            LastPart: "See all",
                          ),
                        ),
                        /////////////////////////////Card Category Offer //////////////////////

                        GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: all_category_name_list.length,
                          scrollDirection: Axis.vertical,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2
                              ),
                          itemBuilder: ((context, index) {
                            return GestureDetector(
                                onTap: () {
                                  Provider.of<All_SubCategory_Product_Provider>(context,listen: false).getBrand(context,int.parse("${all_category_name_list[index].productCategorySlNo}"));
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => SubCategoryProduct(
                                    Categoryname: all_category_name_list[index].productCategoryName,
                                    category_Idi:int.parse("${all_category_name_list[index].productCategorySlNo}"),
                                  ),));
                                },
                                child: Card(
                                  elevation: 8,
                                  child: Container(
                                    padding: EdgeInsets.all(3),
                                    height: 100,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration:const BoxDecoration(
                                              image: DecorationImage(image: AssetImage("images/profile.jpg"),fit: BoxFit.fill),
                                                ),
                                             child:Image.network("https://bigbuy.com.bd/uploads/categories/${all_category_name_list[index].productCategoryImage}" ?? "",fit: BoxFit.fill),
                                          ),
                                        ),
                                        Container(
                                         child:Text(
                                            "${all_category_name_list[index].productCategoryName}",
                                            style:TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                                letterSpacing: 1),
                                          ),
                                          alignment: Alignment.center,
                                          color: Color(0xff076BD6),
                                          height: 27,
                                        ),
                                      ],
                                    ),
                                  ),
                                ));
                          }),
                        )
                      ],
                    ),
                    ////////////////////// Category Part /////////////////
                    const SizedBox(height: 10),
                  ],
                );
              }, childCount: 1),
            ),
          ),
          /////////////////// New Arrivel bar//////////////////
          SliverToBoxAdapter(
            child: Container(
                width: double.infinity,
                padding: padding,
                child: CustomSectionPart(
                  FristPart: "New Arrivel",
                  LastPart: "See all",
                )),
          ),
          ////////////////////////New Arrivel section//////////////////////
          SliverToBoxAdapter(
            child: Container(
              padding: padding,
              width: double.infinity,
              child: GridView.builder(
                shrinkWrap: true,
                physics:const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                gridDelegate:const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 115,
                  mainAxisSpacing: 15,
                  crossAxisSpacing: 15,
                  mainAxisExtent: 115,
                ),
                 itemCount:All_new_arrivel_product_list.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => One_Product_Details(
                         qty: All_new_arrivel_product_list[index].qty,
                          mainImage: All_new_arrivel_product_list[index].mainImage,
                          mainPrice: All_new_arrivel_product_list[index].mainPrice,
                          cashbackAmount: All_new_arrivel_product_list[index].cashbackAmount,
                          productName: All_new_arrivel_product_list[index].productName,
                          productCode: All_new_arrivel_product_list[index].productCode,
                          productDescription: All_new_arrivel_product_list[index].productDescription,
                          salePrice: All_new_arrivel_product_list[index].salePrice,
                        productSlNo: All_new_arrivel_product_list[index].productSlNo,
                      ),));
                    },
                    child: Card(
                      elevation: 8,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: [
                            Expanded(
                                flex: 5,
                                child: Container(
                                  height: double.infinity,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image:const DecorationImage(
                                      image:  AssetImage(
                                          "images/american_express.png"),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  child: Image.network("https://bigbuy.com.bd/uploads/products/small_image/${All_new_arrivel_product_list[index].mainImage}",fit: BoxFit.fill,),
                                )),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${All_new_arrivel_product_list[index].productName}",
                                  style:const TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  maxLines: 1,
                                ),
                                Row(
                                children: [
                                    const Text(
                                       "  Price : ",
                                       style: TextStyle(
                                         fontSize: 12,
                                         fontWeight: FontWeight.bold,
                                       ),
                                     ),
                                   Text(
                                      "${All_new_arrivel_product_list[index].salePrice}",
                                      style:const TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  height: 15,
                                  decoration:const BoxDecoration(
                                    color:Color(0xff076BD6),
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    ),
                                  ),
                                  alignment: Alignment.center,

                                  child:const Text(
                                    "Add to Cart",
                                    style:TextStyle(
                                      fontSize: 11,
                                        color: Colors.white),
                                  ),
                                )
                              ],
                            ),

                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          ////////////////////////Hot deal bar//////////////////////
          // SliverToBoxAdapter(
          //   child: Padding(
          //     padding: padding,
          //     child: CustomSectionPart(
          //       FristPart: "Hot Deal",
          //       LastPart: "See all",
          //     ),
          //   ),
          // ),
          // ////////////////////////////Hot deal section/////////////////////////
          // SliverToBoxAdapter(
          //   child: Container(
          //     padding: padding,
          //     // height: 310,
          //     width: double.infinity,
          //     child: GridView.builder(
          //       shrinkWrap: true,
          //       physics: NeverScrollableScrollPhysics(),
          //       scrollDirection: Axis.vertical,
          //       gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          //         maxCrossAxisExtent: 115,
          //         mainAxisSpacing: 15,
          //         crossAxisSpacing: 15,
          //         mainAxisExtent: 115,
          //       ),
          //       itemCount: HotDelaProductList.length,
          //       itemBuilder: (context, index) {
          //         return InkWell(
          //           onTap: () {
          //             Navigator.push(context, MaterialPageRoute(builder: (context) => One_Product_Details(
          //               mainImage: HotDelaProductList[index].mainImage,
          //               mainPrice: HotDelaProductList[index].mainPrice,
          //               cashbackAmount: HotDelaProductList[index].cashbackAmount,
          //               productName: HotDelaProductList[index].productName,
          //               productCode: HotDelaProductList[index].productCode,
          //               productDescription: HotDelaProductList[index].productDescription,
          //               salePrice: HotDelaProductList[index].salePrice,
          //
          //             ),));
          //           },
          //           child: Home_HotDeal_Section(
          //             cashbackAmount: HotDelaProductList[index].cashbackAmount,
          //             productCode: HotDelaProductList[index].productCode,
          //             salePrice: HotDelaProductList[index].salePrice,
          //             productDescription: HotDelaProductList[index].productDescription,
          //             mainImage: HotDelaProductList[index].mainImage,
          //             mainPrice: HotDelaProductList[index].mainImage,
          //             productName: HotDelaProductList[index].productName,
          //             cashbackPercent: HotDelaProductList[index].cashbackAmount,
          //           ),
          //         );
          //       },
          //     ),
          //   ),
          // ),
          /////////////////////////////All product Bar///////////////////
          SliverToBoxAdapter(
            child: Container(
                height: 100,
                width: double.infinity,
                padding: padding,
                child: CustomSectionPart(
                  FristPart: "All Product",
                  LastPart: "See all",
                )),
          ),
          //// ////////////////////////All Product card//////////////////////
          SliverPadding(
            padding: padding,
            sliver: SliverGrid(
                gridDelegate:const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 185,
                  mainAxisSpacing: 24,
                  crossAxisSpacing: 16,
                  mainAxisExtent: 285,
                ),
                delegate: SliverChildBuilderDelegate(
                    childCount: all_productlist.length,
                    (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => One_Product_Details(
                        // qty: all_productlist[index].qty,
                        cashbackPercent: all_productlist[index].cashbackPercent,
                        productCode: all_productlist[index].productCode,
                        mainImage: all_productlist[index].mainImage,
                        mainPrice: all_productlist[index].mainPrice,
                        productName: all_productlist[index].productName,
                        salePrice: all_productlist[index].salePrice,
                        productDescription: all_productlist[index].productDescription,
                        cashbackAmount: all_productlist[index].cashbackAmount,
                        qty: all_productlist[index].qty,
                        productSlNo: all_productlist[index].productSlNo,
                      ),));
                    },
                    child: Card(
                      elevation: 8.0,
                      child: Custom_Card_Page(
                        productSlNo: "${all_productlist[index].productSlNo}",
                        qty: "${all_productlist[index].qty}",
                        mainImage:"https://bigbuy.com.bd/uploads/products/small_image/${all_productlist[index].mainImage}" ,
                        productName:"${all_productlist[index].productName}",
                        mainPrice: "${all_productlist[index].mainPrice}",
                        productDescription:"${all_productlist[index].productDescription}" ,
                        salePrice: "${all_productlist[index].salePrice}",
                        productCode: "${all_productlist[index].productCode}",
                        cashbackPercent: "${all_productlist[index].cashbackPercent}",
                        cashbackAmount: "${all_productlist[index].cashbackAmount}",
                       ),
                    ),
                   );
                  },
                ),
               ),
             ),
          ////////////////////////////Footer Area ///////////////////////
       //    SliverToBoxAdapter(
       // child: BigBuyFooter(),
       //    ),
        ],
      ),
    );
  }

  int selectIndex = 0;
  Color? color = Colors.black;
}
